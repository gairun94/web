###### 玩转金刚梯>金刚字典>

### 金刚号梯

- <strong> 金刚号梯 </strong>是由[ 金刚公司 ](/LadderFree/kkDictionary/Atozitpro.md)研发、销售的一款[ 金刚梯 ](/LadderFree/kkDictionary/KKLadder.md)的统称
- 其产品形态是由4条参数构成的[ 一套参数 ](/LadderFree/kkDictionary/KKIDsParameters.md)
- 其中最重要的参数是[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)，故名<strong> 金刚号梯 </strong>
- [ 金刚用户 ](/LadderFree/kkDictionary/KKUser.md)只需把[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)派送的[ 这套参数 ](/LadderFree/kkDictionary/KKIDsParameters.md)按照《配置说明》[ 正确配入 ](/LadderFree/kkDictionary/ConsiderationsWhileConfigureKKID.md) [智能设备 ](/LadderFree/A.md)，该设备即具有[ 翻墙 ](/LadderFree/kkDictionary/OverTheWall.md)本领，而勿需安装[ 金刚 ](/LadderFree/kkDictionary/Atozitpro.md)或其它品牌的任何客户端App软件
- <strong> 金刚号梯 </strong>共分为两类
  - [ 万能金刚号梯 ](/LadderFree/kkDictionary/KKLadderKKIDMultipurpose.md)
  - [ 普通金刚号梯 ](/LadderFree/kkDictionary/KKLadderKKIDSinglepurpose.md)



#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)


